﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml.Linq;

namespace Practicum4
{
    internal class Program
    {
        private static void Main()
        {
            var track1 = new Track("Dream Theater", "6:00",new TimeSpan(0,5,31));
            var track2 = new Track("Dream Theater", "Caught in a Web", new TimeSpan(0,5,58));
            var track3 = new Track("Dream Theater", "Innocence Faded", new TimeSpan(0,5,34));
            var cd = new Cd("Dream Theater", "Awake");
            cd.AddTrack(track1, track2, track3);
            Console.WriteLine($"{cd.ToXml()}\n");
            Console.ReadKey();
            XDocument myXmlDoc;
            using (var wc = new WebClient())
            {
                var xmlString = wc.DownloadString(
                    @"http://ws.audioscrobbler.com/2.0/?method=album.getInfo&album=awake&artist=Dream%20Theater&api_key=b5cbf8dcef4c6acfc5698f8709841949");
                myXmlDoc = XDocument.Parse(xmlString);
            }
            var trackList =
                myXmlDoc.Descendants("tracks")
                    .Elements("track")
                    .Where(
                        track =>
                        {
                            var xElement = track.Element("name");
                            return
                                xElement != null && (!cd.TrackExists(xElement.Value,
                                    track.Element("artist")?.Element("name")?.Value) &&
                                                                  !xElement.Value.Contains("LP Version"));
                        })
                    .Select(
                        track => new Track(track.Element("artist")?.Element("name")?.Value,
                            track.Element("name")?.Value, 
                            TimeSpan.FromSeconds(Convert.ToDouble(track.Element("duration")?.Value))
                            )).ToList();
            
            trackList.ForEach(track => cd.AddTrack(track));
            Console.WriteLine(cd.ToXml());
            Console.ReadKey();
            
        }
    }

    internal class Cd
    {
        public readonly string Artist;
        public readonly string Name;
        public List<Track> Tracks { get; }

        public Cd(string artist, string name)
        {
            Artist = artist;
            Name = name;
            Tracks = new List<Track>();
        }

        public void AddTrack(params Track[] tracks)
        {
            tracks.Where(track => !TrackExists(track.Title, track.Artist))
                .ToList()
                .ForEach(track => Tracks.Add(track));
        }

        public void RemoveTrack(params Track[] tracks)
        {
            tracks.Where(track => TrackExists(track.Title, track.Artist))
                .ToList()
                .ForEach(track => Tracks.Remove(track));
        }

        public bool TrackExists(string trackTitle, string trackArtist)
        {
            return Tracks.Any(track => track.Title.Equals(trackTitle)
            && track.Artist.Equals(trackArtist));
        }

        public XDocument ToXml()
        {
            return new XDocument(new XElement("cd",
                new XAttribute("artist", Artist),
                new XAttribute("name", Name),
                new XElement("tracks",
                from track in Tracks
                select track.ToXml()
                )));
        }
    }

    internal class Track
    {
        public readonly string Artist;
        public readonly string Title;
        public readonly TimeSpan Length;

        public Track(string artist, string title, TimeSpan length)
        {
            Artist = artist;
            Title = title;
            Length = length;
        }

        public XElement ToXml()
        {
            return new XElement("track",
                new XElement("artist", Artist),
                new XElement("title", Title),
                new XElement("length", Length.ToString()));
        }
    }
}
