﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Practicum3
{
    internal class Program
    {
        private static void Main()
        {
            const string path = @"C:\tmp\randomtext.txt";
            GetWords(path, s => s.StartsWith("a")).ToList().ForEach(word => Console.Write("{0};", word));
            Console.ReadKey();
            Console.WriteLine("\n");
            var array = GetWords(path, s => s.StartsWith("b")).ToArray();
            Array.Sort(array, (x, y) => x.Length.CompareTo(y.Length));
            array.ToList().ForEach(word => Console.Write("{0};", word));
            Console.ReadKey();
        }


        private static IEnumerable<string> GetWords(string path, Func<string, bool> compare)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine("File Not Found!  ");
                yield break;
            }
            //var file = File.ReadAllText(path);
            //var words = file.Split(new[] {' ', '.', ',', '\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);
            //foreach (var word in words.Where(compare.Invoke))
            //{
            //    yield return word;
            //}
            var file = File.ReadAllLines(path);
            foreach (var word in file.Select(s => s.Split(new[] { ' ', '.', ',' }, StringSplitOptions.RemoveEmptyEntries))
                .SelectMany(words => words.Where(compare.Invoke)))
            {
                yield return word;
            }
        }
    }
}
